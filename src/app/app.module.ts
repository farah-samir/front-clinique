import { PieComponent } from './shared/widgets/pie/pie.component';
import { CardComponent } from './shared/widgets/card/card.component';
import { AreaComponent } from './shared/widgets/area/area.component';
import { BrowserModule } from '@angular/platform-browser';
import {NgModule } from '@angular/core';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

import { Observable } from 'rxjs';

import { CommonModule } from '@angular/common';

import { FlatpickrModule } from 'angularx-flatpickr';
import {  DateAdapter } from 'angular-calendar';

import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';


import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatStepperModule} from '@angular/material/stepper';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatSidenavModule, MatDividerModule, 
  MatCardModule, MatPaginatorModule, MatTableModule,
   MatIconModule, MatMenuModule, MatListModule


} from '@angular/material';
import { OrderModule } from 'ngx-order-pipe';

import { NgxPaginationModule } from 'ngx-pagination';

import { ReactiveFormsModule } from '@angular/forms';
import {DatePipe} from '@angular/common';
import { EditorComponent } from './editor/editor.component';

import { AngularEditorModule } from '@kolkov/angular-editor';
import { EditorModule } from '@tinymce/tinymce-angular';
import { TeComponent } from './te/te.component';
import { NgxEditorModule } from 'ngx-editor';
import { LoginFormComponent } from './login-form/login-form.component';
import { UserComponent } from './user/user.component';
import { RequestRestComponent } from './components/password/request-rest/request-rest.component';
import { ResponseRestComponent } from './components/password/response-rest/response-rest.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthComponent } from './components/auth/auth.component';
import { NotesComponent } from './notes/notes.component';

import { JarwisService } from './services/jarwis.service';
import { TokenService } from './services/token.service';

import { AuthService } from './services/auth.service';
import { AfterLoginService } from './services/after-login.service';
import { BeforeLoginService } from './services/before-login.service';
import { AuthGuard } from './auth.guard';
import { SnotifyModule, SnotifyService, ToastDefaults } from 'ng-snotify';
import { AccuielComponent } from './accuiel/accuiel.component';





import { AutoCompleteModule } from 'primeng/autocomplete';
import { TestformulaireComponent } from './testformulaire/testformulaire.component';
import  {TreeTableModule , SharedModule,ButtonModule,DialogModule, InputTextModule ,CalendarModule, 
  DropdownModule, ConfirmDialogModule,
  TabViewModule, ConfirmationService, PanelModule

} from 'primeng/primeng';
import { TableModule } from 'primeng/table';


import { FullCalendarModule } from '@fullcalendar/angular';



import { TopnavabrComponent } from './topnavabr/topnavabr.component';
import { NavigateComponent } from './navigate/navigate.component';
import { CategoryComponent } from './pages/category/category.component';
import { AchatComponent } from './pages/achat/achat.component';
import { VendeComponent } from './pages/vende/vende.component';
import { StockComponent } from './pages/stock/stock.component';
import { PaiementComponent } from './pages/paiement/paiement.component';
import { OperationComponent } from './pages/operation/operation.component';
import { EntreComponent } from './pages/entre/entre.component';
import { RendezComponent } from './pages/rendez/rendez.component';
import { AnalyseComponent } from './pages/analyse/analyse.component';
import { InfoComponent } from './pages/info/info.component';
import { PaieComponent } from './pages/paie/paie.component';
import { SalarieComponent } from './pages/salarie/salarie.component';
import { DepartementComponent } from './pages/departement/departement.component';
import { ShowCategoryComponent } from './shows/show-category/show-category.component';
import { CategoryshowComponent } from './eyes/categoryshow/categoryshow.component';
import { ShowRendezComponent } from './shows/show-rendez/show-rendez.component';
import { ShowVenduComponent } from './shows/show-vendu/show-vendu.component';
import { SalComponent } from './pages/sal/sal.component';
import { ShowsalarieComponent } from './shows/showsalarie/showsalarie.component';
import { ShowAnalyseComponent } from './shows/show-analyse/show-analyse.component';
import { ShowStockComponent } from './shows/show-stock/show-stock.component';
import { ShowOperationComponent } from './shows/show-operation/show-operation.component';
import { ShowDepartementComponent } from './shows/show-departement/show-departement.component';
import { ShowPaieComponent } from './shows/show-paie/show-paie.component';
import { ShowPaiementComponent } from './shows/show-paiement/show-paiement.component';
import { ShowCongeComponent } from './shows/show-conge/show-conge.component';
import { ShowSalarieComponent } from './shows/show-salarie/show-salarie.component';
import { ShowInfoComponent } from './shows/show-info/show-info.component';
import { DefaultComponent } from './layouts/default/default.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { PostsComponent } from './modules/posts/posts.component';

import {MatToolbarModule} from '@angular/material/toolbar';

import { FlexLayoutModule } from '@angular/flex-layout';

import { HighchartsChartModule } from 'highcharts-angular';


const appRoutes: Routes = [
 

{path:'top',
canActivate: [AfterLoginService],
component: TopnavabrComponent},

{path:'nav',
canActivate: [AfterLoginService],
component: NavigateComponent},


  {path:'te',
  canActivate: [AfterLoginService],
component: TeComponent},

 

  {path:'accuiel',
    canActivate: [AfterLoginService],
  component: AccuielComponent},



// BUG: l'affichage de table pas de id mais pour modifie , il faut obligatoire mettre l'id devant (router )
// BUG: comme home/:id





        {path:'editor',canActivate: [AfterLoginService],component:EditorComponent},
        {path:'editor/:id',canActivate: [AfterLoginService],component:EditorComponent},
        {path:'te',canActivate: [AfterLoginService],component:TeComponent},
       {path:'notes',canActivate: [AfterLoginService], component: NotesComponent},
       {path: 'login', component: LoginComponent},
       {path: 'signup',component: SignupComponent },


];

@NgModule({
  declarations: [
    PieComponent,
    CardComponent,
    AppComponent,
    AreaComponent,
     EditorComponent,
     TeComponent,
     NotesComponent,
     LoginFormComponent,
     UserComponent,
     RequestRestComponent,
     ResponseRestComponent,
     NavbarComponent,
     LoginComponent,
     SignupComponent,
     AuthComponent,
     AccuielComponent,
     TestformulaireComponent,
     TopnavabrComponent,
     NavigateComponent,
     CategoryComponent,
     AchatComponent,
     VendeComponent,
     StockComponent,
     PaiementComponent,
     OperationComponent,
     EntreComponent,
     RendezComponent,
     AnalyseComponent,
     InfoComponent,
     PaieComponent,
     SalarieComponent,
     DepartementComponent,
     ShowCategoryComponent,
     CategoryshowComponent,
     ShowRendezComponent,
     ShowVenduComponent,
     SalComponent,
     ShowsalarieComponent,
     ShowAnalyseComponent,
     ShowStockComponent,
     ShowOperationComponent,
     ShowDepartementComponent,
     ShowPaieComponent,
     ShowPaiementComponent,
     ShowCongeComponent,
     ShowSalarieComponent,
     ShowInfoComponent,
     DefaultComponent,
     DashboardComponent,
     HeaderComponent,
     FooterComponent,
     SidebarComponent,
     PostsComponent,
     

  ],
  imports: [
    HighchartsChartModule,
    FlexLayoutModule,
    BrowserModule,
    HttpClientModule,
        BrowserModule,
        RouterModule.forRoot(appRoutes),
        FormsModule,
        BrowserAnimationsModule,
        MatStepperModule,
        NoopAnimationsModule,
        MatCheckboxModule,
        NgxPaginationModule,
        OrderModule,
        NgbPaginationModule, NgbAlertModule,
        ReactiveFormsModule,
        CommonModule,
        NgbModalModule,
        FlatpickrModule.forRoot(),
      
     AngularEditorModule,
     EditorModule,
     NgxEditorModule,
     AppRoutingModule,
     BrowserAnimationsModule,
     FullCalendarModule,
     AutoCompleteModule,
     TreeTableModule , SharedModule,ButtonModule,DialogModule,InputTextModule,
     CalendarModule,DropdownModule,
     PanelModule,TabViewModule,TableModule,
     MatIconModule,
     MatSidenavModule,
     MatDividerModule,
     MatCardModule,
     MatPaginatorModule,
     MatTableModule,

     MatMenuModule, MatListModule,
     MatToolbarModule,
       SnotifyModule
       


  ],
  providers: [DatePipe , ConfirmDialogModule,ConfirmationService, JarwisService,
  TokenService,AuthService, AfterLoginService,BeforeLoginService,AuthGuard,
  { provide: 'SnotifyToastConfig', useValue: ToastDefaults },
  SnotifyService

],
  exports: [MatButtonModule, MatCheckboxModule ],
  bootstrap: [AppComponent]
})
export class AppModule { }
