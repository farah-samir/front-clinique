import { Component, OnInit } from '@angular/core';
import { Editor } from '../interface/editor';
import { EditorService } from '../services/editor.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  nom:string;
   editors:Editor[];

  constructor(private editorService:EditorService) {
// BUG: had l connstructeur bach tjib table kamla
  this.getService();
  }

  ngOnInit() {
  }
  search(){
    if(this.nom != ""){
      this.editors= this.editors.filter(res=>{
        return res.nom.toLocaleLowerCase().match(this.nom.toLocaleLowerCase());
      }
    );
  }else if (this.nom == ""){
 this.getService();
  }

  }

  getService(){
    this.editorService.get().subscribe((data:Editor[])=>{
     this.editors = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.editorService.destroy(id).subscribe((data:Editor[])=>{
       this.editors = data;
       this.getService();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }

}
