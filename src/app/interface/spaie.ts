export interface Spaie {
    id?: number;
    salarie_id: number;
    specialite:string;
  
    salaire:string;
    prime:string;
    salaire_net:number;
    cin:string;
    prenom:string;
}
