export interface Stock {
    id?: number;
    nomid: string;
    achat_id:string;
    categorie_id:string;
    prix:string;
    nobrachat:string;
    tva:string;
    montant:number;
}
