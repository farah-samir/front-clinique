export interface Vendu {
    id?: number;
    categorie_id: string;
    nom:string;
    prix:number;
    nobrachat:number;
}
