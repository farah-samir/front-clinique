export interface User {
  id?: number;
  name: string;
  email: string;
  password:string;
  c_password:string;
}
