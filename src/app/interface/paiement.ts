export interface Paiement {

    id?: number;
    operation_id: string;
    nomid:string;
    pharmacie:string;
    monatnt_operation:string;
    dates:Date;
    montant_departement:string;
    montant_globale:number;
}