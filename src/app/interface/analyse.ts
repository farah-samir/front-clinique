export interface Analyse {
    id?: number;
    rendez_id: string;
    nomid:string;
    depart_id: string;
    chambre: number;
    test: string;
    date_red_v: Date;

}
