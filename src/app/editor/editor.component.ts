import { Component, OnInit } from '@angular/core';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { Editor } from '../interface/editor';
import { EditorService } from '../services/editor.service';
import { Observable } from 'rxjs';
import { ActivatedRoute ,Router} from '@angular/router';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
   htmlStr: string = '<strong>The Tortoise</strong> &amp; the Hare';
   htmlContent = '';
   nom='';


   editor:Editor = {
   nom: null,
   htmlContent:null,

   };
   editing : boolean = false;
   editors:Editor[];
   id: any;

  constructor(private editorService:EditorService , private activatedRoute:ActivatedRoute , private router:Router ) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.editing = true;
    this.editorService.get().subscribe((data:Editor[])=>{
      this.editors=data;
    this.editor = this.editors.find((m)=>{return m.id == this.id});
    console.log(this.editor);
    },(error)=>{
      console.log(error);
    });
  }else {
    this.editing = false;
  }

   }

  ngOnInit() {
  }
  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: '25rem',
    minHeight: '5rem',
    placeholder: 'Enter text here...',
    translate: 'no',
    uploadUrl: 'v1/images', // if needed
    customClasses: [ // optional

    ]
  };

  Editor(){
    if (this.editing){
      this.editorService.put(this.editor).subscribe((data)=>{
        alert('enregistrement actualisé');
        this.router.navigate(['/notes']);
       
        console.log(data);
      },(error)=>{
        console.log(error);
        alert('eroor');
      });


    }else {

      this.editorService.save(this.editor).subscribe((data)=>{
        alert('enregistrement save');
        this.router.navigate(['/notes']);
        
        console.log(data);
      },(error)=>{
        console.log(error);
        alert('eroor');
      });

    }

  }


}
