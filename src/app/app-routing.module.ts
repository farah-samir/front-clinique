import { PostsComponent } from './modules/posts/posts.component';
import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { CategoryComponent } from './pages/category/category.component';
import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { RequestRestComponent } from './components/password/request-rest/request-rest.component';
import { ResponseRestComponent } from './components/password/response-rest/response-rest.component';
import { AuthComponent } from './components/auth/auth.component';
import { AfterLoginService } from './services/after-login.service';
import { BeforeLoginService } from './services/before-login.service';
import { Observable } from 'rxjs';
import { AchatComponent } from './pages/achat/achat.component';
import { VendeComponent } from './pages/vende/vende.component';
import { StockComponent } from './pages/stock/stock.component';
import { PaiementComponent } from './pages/paiement/paiement.component';
import { OperationComponent } from './pages/operation/operation.component';
import { EntreComponent } from './pages/entre/entre.component';
import { RendezComponent } from './pages/rendez/rendez.component';
import { AnalyseComponent } from './pages/analyse/analyse.component';
import { InfoComponent } from './pages/info/info.component';
import { PaieComponent } from './pages/paie/paie.component';
import { SalarieComponent } from './pages/salarie/salarie.component';
import { DepartementComponent } from './pages/departement/departement.component';
import { ShowCategoryComponent } from './shows/show-category/show-category.component';
import { CategoryshowComponent } from './eyes/categoryshow/categoryshow.component';
import { ShowRendezComponent } from './shows/show-rendez/show-rendez.component';
import { ShowVenduComponent } from './shows/show-vendu/show-vendu.component';

import { SalComponent } from './pages/sal/sal.component';
import { ShowSalarieComponent } from './shows/show-salarie/show-salarie.component';

import { ShowAnalyseComponent } from './shows/show-analyse/show-analyse.component';

import { ShowStockComponent } from './shows/show-stock/show-stock.component';
import { ShowOperationComponent } from './shows/show-operation/show-operation.component';

import { ShowInfoComponent } from './shows/show-info/show-info.component';
import { ShowDepartementComponent } from './shows/show-departement/show-departement.component';

import { ShowPaiementComponent } from './shows/show-paiement/show-paiement.component';

import { ShowCongeComponent } from './shows/show-conge/show-conge.component';
import { ShowPaieComponent } from './shows/show-paie/show-paie.component';
import { DefaultComponent } from './layouts/default/default.component';




const appRoutes: Routes = [

  {
    path: '',
    component: DefaultComponent,
    children: [{
      path: '',
      component: DashboardComponent
    }] },

    {
      path: 'posts',
      component: PostsComponent
    },

  {
    path: 'login',component: LoginComponent,canActivate: [BeforeLoginService]},
  {
    path: 'signup',
    component: SignupComponent,
  canActivate: [BeforeLoginService]


  },




  {
    path: 'request-password-reset',
    component: RequestRestComponent,
  canActivate: [BeforeLoginService]


  },
  {
    path: 'response-password-reset',
    component: ResponseRestComponent,
  canActivate: [BeforeLoginService]
  },

  { path: 'categories',component: CategoryComponent, canActivate: [AfterLoginService]},
  { path: 'categories/:id',component: CategoryComponent, canActivate: [AfterLoginService]},
 

  { path: 'sal',component: SalComponent, canActivate: [AfterLoginService]},
  { path: 'sal/:id',component: SalComponent, canActivate: [AfterLoginService]},

  { path: 'showconge',component: ShowCongeComponent, canActivate: [AfterLoginService]}, 

  { path: 'spaie',component: PaieComponent, canActivate: [AfterLoginService]},
  { path: 'spaie/:id',component: PaieComponent, canActivate: [AfterLoginService]},

  { path: 'showpaie',component: ShowPaieComponent, canActivate: [AfterLoginService]},


  { path: 'achats',component: AchatComponent, canActivate: [AfterLoginService]},

  { path: 'Vendus',component: VendeComponent, canActivate: [AfterLoginService]}, 
  { path: 'Vendus/:id',component: VendeComponent, canActivate: [AfterLoginService]}, 

  { path: 'stocks',component: StockComponent, canActivate: [AfterLoginService]}, 
  { path: 'stocks/:id',component: StockComponent, canActivate: [AfterLoginService]}, 

  { path: 'paiements',component: PaiementComponent, canActivate: [AfterLoginService]}, 
  { path: 'paiements/:id',component: PaiementComponent, canActivate: [AfterLoginService]}, 
  { path: 'shpowpaiement',component: ShowPaiementComponent, canActivate: [AfterLoginService]}, 

  { path: 'operations',component: OperationComponent, canActivate: [AfterLoginService]}, 
  { path: 'operations/:id',component: OperationComponent, canActivate: [AfterLoginService]}, 

  { path: 'Entre',component: EntreComponent, canActivate: [AfterLoginService]}, 

  { path: 'rendez',component: RendezComponent, canActivate: [AfterLoginService]},
  { path: 'rendez/:id',component: RendezComponent, canActivate: [AfterLoginService]},

  { path: 'radio',component: AnalyseComponent, canActivate: [AfterLoginService]}, 
  { path: 'radio/:id',component: AnalyseComponent, canActivate: [AfterLoginService]}, 

  { path: 'maladies',component: InfoComponent, canActivate: [AfterLoginService]}, 
  { path: 'maladies/:id',component: InfoComponent, canActivate: [AfterLoginService]}, 

  { path: 'paie',component: PaieComponent, canActivate: [AfterLoginService]}, 
  { path: 'salarie',component: SalarieComponent, canActivate: [AfterLoginService]}, 
  { path: 'salarie/:id',component: SalarieComponent, canActivate: [AfterLoginService]}, 
  { path: 'showsalaries',component: ShowSalarieComponent, canActivate: [AfterLoginService]}, 

  { path: 'departement',component: DepartementComponent, canActivate: [AfterLoginService]}, 
  { path: 'departement/:id',component: DepartementComponent, canActivate: [AfterLoginService]}, 
  { path: 'showdepartement',component: ShowDepartementComponent, canActivate: [AfterLoginService]}, 

  { path: 'showCatgeorie',component: ShowCategoryComponent, canActivate: [AfterLoginService]}, 
  { path: 'showrendez',component: ShowRendezComponent, canActivate: [AfterLoginService]}, 
  { path: 'showvendu',component: ShowVenduComponent, canActivate: [AfterLoginService]}, 
  { path: 'showstock',component: ShowStockComponent, canActivate: [AfterLoginService]}, 

  { path: 'showsinfo',component: ShowInfoComponent, canActivate: [AfterLoginService]}, 

  { path: 'showanalyse',component: ShowAnalyseComponent, canActivate: [AfterLoginService]}, 
  { path: 'showoperation',component: ShowOperationComponent, canActivate: [AfterLoginService]},
  

  { path: 'categoryshow',component: CategoryshowComponent, canActivate: [AfterLoginService]}, 

 
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)

  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
