import { Component, OnInit } from '@angular/core';
import { StockService } from '../../services/stock.service';
import { Stock } from './../../interface/stock';

@Component({
  selector: 'app-show-stock',
  templateUrl: './show-stock.component.html',
  styleUrls: ['./show-stock.component.css']
})
export class ShowStockComponent implements OnInit {

  nom:string;
 stocks: Stock[];
  
  resultSearch : any[] =[];
  constructor(private salService:StockService) {
this.getService();
  }



getService(){
  this.salService.get().subscribe((data:Stock[])=>{
   this.stocks = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}



ngOnInit() {
}
destroy(id){
 if(confirm('vous etes sure de suprimie ce personne')){
   this.salService.destroy(id).subscribe((data:Stock[])=>{
    this.stocks = data;
    this.getService();
    console.log(data)
       }, (error)=>{
          console.log(error);
                alert('error');
                   });
 }
}

}
