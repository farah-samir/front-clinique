import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAnalyseComponent } from './show-analyse.component';

describe('ShowAnalyseComponent', () => {
  let component: ShowAnalyseComponent;
  let fixture: ComponentFixture<ShowAnalyseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAnalyseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAnalyseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
