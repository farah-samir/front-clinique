import { Component, OnInit } from '@angular/core';
import { AnalyseService } from '../../services/analyse.service';
import { Analyse } from './../../interface/analyse';

@Component({
  selector: 'app-show-analyse',
  templateUrl: './show-analyse.component.html',
  styleUrls: ['./show-analyse.component.css']
})
export class ShowAnalyseComponent implements OnInit {

  nom:string;
  analyses: Analyse[];
  
  resultSearch : any[] =[];
  constructor(private salService:AnalyseService) {
this.getService();
  }



getService(){
  this.salService.get().subscribe((data:Analyse[])=>{
   this.analyses = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}



ngOnInit() {
}
destroy(id){
 if(confirm('vous etes sure de suprimie ce personne')){
   this.salService.destroy(id).subscribe((data:Analyse[])=>{
    this.analyses = data;
    this.getService();
    console.log(data)
       }, (error)=>{
          console.log(error);
                alert('error');
                   });
 }
}
}
