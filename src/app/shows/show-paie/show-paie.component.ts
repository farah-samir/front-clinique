import { Component, OnInit } from '@angular/core';
import { SpaieService } from '../../services/spaie.service';

import { Spaie } from './../../interface/spaie';


@Component({
  selector: 'app-show-paie',
  templateUrl: './show-paie.component.html',
  styleUrls: ['./show-paie.component.css']
})
export class ShowPaieComponent implements OnInit {

  nom:string;
  spaies: Spaie[];
  
  resultSearch : any[] =[];
  constructor(private salService:SpaieService) {
this.getService();
  }



getService(){
  this.salService.get().subscribe((data:Spaie[])=>{
   this.spaies = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}



ngOnInit() {
}
destroy(id){
 if(confirm('vous etes sure de suprimie ce personne')){
   this.salService.destroy(id).subscribe((data:Spaie[])=>{
    this.spaies = data;
    this.getService();
    console.log(data)
       }, (error)=>{
          console.log(error);
                alert('error');
                   });
 }
}
}
