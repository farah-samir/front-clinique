import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowVenduComponent } from './show-vendu.component';

describe('ShowVenduComponent', () => {
  let component: ShowVenduComponent;
  let fixture: ComponentFixture<ShowVenduComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowVenduComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowVenduComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
