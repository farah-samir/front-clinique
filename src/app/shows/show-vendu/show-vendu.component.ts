import { Component, OnInit } from '@angular/core';
import { VenduService } from '../../services/vendu.service';
import { Vendu } from './../../interface/vendu';
import { CategorieService } from '../../services/categorie.service';
import { Categorie } from './../../interface/categorie';

@Component({
  selector: 'app-show-vendu',
  templateUrl: './show-vendu.component.html',
  styleUrls: ['./show-vendu.component.css']
})
export class ShowVenduComponent implements OnInit {

  cate:string;
  vendus: Vendu[];
  categories: Categorie[];

  categorie:Categorie = {
    nom: null,
   
    };


  constructor(private categoryService:CategorieService , private  venduService:VenduService) {
  
    this.getService();
    this.getServic();
   }


   search(){
     if(this.cate != ""){
       this.vendus= this.vendus.filter(res=>{
         return res.nom.toLocaleLowerCase().match(this.cate.toLocaleLowerCase());
       }
     );
   }else if (this.cate == ""){
this.getService();
this.getServic();
   }

   }
  getService(){
    this.venduService.get().subscribe((data:Vendu[])=>{
     this.vendus = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }


  getServic(){
    this.categoryService.get().subscribe((data:Categorie[])=>{
     this.categories = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }


  ngOnInit() {

  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.venduService.destroy(id).subscribe((data:Vendu[])=>{
       this.vendus = data;
       this.getService();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }

}
