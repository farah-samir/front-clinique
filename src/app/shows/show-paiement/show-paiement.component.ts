import { Component, OnInit } from '@angular/core';
import { PaiementService } from '../../services/paiement.service';
import { Paiement } from './../../interface/paiement';

@Component({
  selector: 'app-show-paiement',
  templateUrl: './show-paiement.component.html',
  styleUrls: ['./show-paiement.component.css']
})
export class ShowPaiementComponent implements OnInit {

  nom:string;
  paiements: Paiement [];
  
  resultSearch : any[] =[];
  constructor(private salService: PaiementService) {
this.getService();
  }



getService(){
  this.salService.get().subscribe((data:Paiement[])=>{
   this.paiements = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}



ngOnInit() {
}
destroy(id){
 if(confirm('vous etes sure de suprimie ce personne')){
   this.salService.destroy(id).subscribe((data:Paiement[])=>{
    this.paiements = data;
    this.getService();
    console.log(data)
       }, (error)=>{
          console.log(error);
                alert('error');
                   });
 }
}

}
