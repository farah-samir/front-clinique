import { Component, OnInit } from '@angular/core';
import { DepartementService } from '../../services/departement.service';
import { Departement } from './../../interface/departement';

@Component({
  selector: 'app-show-departement',
  templateUrl: './show-departement.component.html',
  styleUrls: ['./show-departement.component.css']
})
export class ShowDepartementComponent implements OnInit {

  cate:string;
  departements: Departement[];
  constructor(private categoryService:DepartementService) {
  this.getCategorye();

   }


   search(){
     if(this.cate != ""){
       this.departements= this.departements.filter(res=>{
         return res.nom_department.toLocaleLowerCase().match(this.cate.toLocaleLowerCase());
       }
     );
   }else if (this.cate == ""){

   }

   }
  getCategorye(){
    this.categoryService.get().subscribe((data:Departement[])=>{
     this.departements = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }
  ngOnInit() {
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.categoryService.destroy(id).subscribe((data:Departement[])=>{
       this.departements = data;
       this.getCategorye();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }

}
