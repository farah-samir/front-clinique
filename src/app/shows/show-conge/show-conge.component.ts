import { Component, OnInit } from '@angular/core';
import { SalService } from '../../services/sal.service';
import { Sal } from './../../interface/sal';

@Component({
  selector: 'app-show-conge',
  templateUrl: './show-conge.component.html',
  styleUrls: ['./show-conge.component.css']
})
export class ShowCongeComponent implements OnInit {

  cate:string;
  sals: Sal[];

  constructor(private categoryService:SalService) {
  this.getCategorye();

   }


   search(){
     if(this.cate != ""){
       this.sals= this.sals.filter(res=>{
         return res.prenom.toLocaleLowerCase().match(this.cate.toLocaleLowerCase());
       }
     );
   }else if (this.cate == ""){
this.getCategorye();
   }

   }
  getCategorye(){
    this.categoryService.get().subscribe((data:Sal[])=>{
     this.sals = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }
  ngOnInit() {
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.categoryService.destroy(id).subscribe((data:Sal[])=>{
       this.sals = data;
       this.getCategorye();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }

}
