import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CategorieService } from '../../services/categorie.service';
import { Categorie } from './../../interface/categorie';

@Component({
  selector: 'app-show-category',
  templateUrl: './show-category.component.html',
  styleUrls: ['./show-category.component.css']
})
export class ShowCategoryComponent implements OnInit {

  cate:string;
  categories: Categorie[];
  constructor(private categoryService:CategorieService) {
  this.getCategorye();

   }


   search(){
     if(this.cate != ""){
       this.categories= this.categories.filter(res=>{
         return res.nom.toLocaleLowerCase().match(this.cate.toLocaleLowerCase());
       }
     );
   }else if (this.cate == ""){
this.getCategorye();
   }

   }
  getCategorye(){
    this.categoryService.get().subscribe((data:Categorie[])=>{
     this.categories = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }
  ngOnInit() {
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.categoryService.destroy(id).subscribe((data:Categorie[])=>{
       this.categories = data;
       this.getCategorye();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }
}
