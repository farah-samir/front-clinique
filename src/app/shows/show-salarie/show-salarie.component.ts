import { Component, OnInit } from '@angular/core';
import { SalarieService } from '../../services/salarie.service';
import { Salarie } from './../../interface/salarie';

@Component({
  selector: 'app-show-salarie',
  templateUrl: './show-salarie.component.html',
  styleUrls: ['./show-salarie.component.css']
})
export class ShowSalarieComponent implements OnInit {

  cate:string;
  salaries: Salarie[];
  constructor(private categoryService:SalarieService) {
  this.getCategorye();

   }


   search(){
     if(this.cate != ""){
       this.salaries= this.salaries.filter(res=>{
         return res.nom.toLocaleLowerCase().match(this.cate.toLocaleLowerCase());
       }
     );
   }else if (this.cate == ""){

   }

   }
  getCategorye(){
    this.categoryService.get().subscribe((data:Salarie[])=>{
     this.salaries = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }
  ngOnInit() {
  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.categoryService.destroy(id).subscribe((data:Salarie[])=>{
       this.salaries = data;
       this.getCategorye();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }


}
