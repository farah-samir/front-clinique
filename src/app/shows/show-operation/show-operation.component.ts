import { Component, OnInit } from '@angular/core';
import { OperationService } from '../../services/operation.service';
import { Operation } from './../../interface/operation';

@Component({
  selector: 'app-show-operation',
  templateUrl: './show-operation.component.html',
  styleUrls: ['./show-operation.component.css']
})
export class ShowOperationComponent implements OnInit {

  nom:string;
  operations: Operation[];
  
  resultSearch : any[] =[];
  constructor(private salService:OperationService) {
this.getService();
  }



getService(){
  this.salService.get().subscribe((data:Operation[])=>{
   this.operations = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}



ngOnInit() {
}
destroy(id){
 if(confirm('vous etes sure de suprimie ce personne')){
   this.salService.destroy(id).subscribe((data:Operation[])=>{
    this.operations = data;
    this.getService();
    console.log(data)
       }, (error)=>{
          console.log(error);
                alert('error');
                   });
 }
}

}
