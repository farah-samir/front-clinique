import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowRendezComponent } from './show-rendez.component';

describe('ShowRendezComponent', () => {
  let component: ShowRendezComponent;
  let fixture: ComponentFixture<ShowRendezComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowRendezComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowRendezComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
