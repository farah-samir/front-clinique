import { Component, OnInit } from '@angular/core';
import { RendezService } from '../../services/rendez.service';
import { Rendez } from './../../interface/rendez';
import { InfoService } from '../../services/info.service';
import { Info } from './../../interface/info';
@Component({
  selector: 'app-show-rendez',
  templateUrl: './show-rendez.component.html',
  styleUrls: ['./show-rendez.component.css']
})
export class ShowRendezComponent implements OnInit {

  cate:string;
  rendezs: Rendez[];
  infos: Info[];

 


  constructor(private rendezService:RendezService , private  infoService:InfoService) {
  
    this.getService();

   }


   search(){
     if(this.cate != ""){
       this.rendezs= this.rendezs.filter(res=>{
         return res.cin.toLocaleLowerCase().match(this.cate.toLocaleLowerCase());
       }
     );
   }else if (this.cate == ""){
this.getService();

   }

   }
  getService(){
    this.rendezService.get().subscribe((data:Rendez[])=>{
     this.rendezs = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }





  ngOnInit() {

  }
  destroy(id){
    if(confirm('vous etes sure de suprimie ce personne')){
      this.rendezService.destroy(id).subscribe((data:Rendez[])=>{
       this.rendezs = data;
       this.getService();
       console.log(data)
          }, (error)=>{
             console.log(error);
                   alert('error');
                      });
    }
  }
}
