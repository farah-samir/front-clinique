import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';
import { Info} from './../../interface/info';

@Component({
  selector: 'app-show-info',
  templateUrl: './show-info.component.html',
  styleUrls: ['./show-info.component.css']
})
export class ShowInfoComponent implements OnInit {

  nom:string;
  infos: Info[];
  
  resultSearch : any[] =[];
  
  constructor(private salService:InfoService) {
this.getService();
  }



getService(){
  this.salService.get().subscribe((data:Info[])=>{
   this.infos = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}



ngOnInit() {
}
destroy(id){
 if(confirm('vous etes sure de suprimie ce personne')){
   this.salService.destroy(id).subscribe((data:Info[])=>{
    this.infos = data;
    this.getService();
    console.log(data)
       }, (error)=>{
          console.log(error);
                alert('error');
                   });
 }
}

}
