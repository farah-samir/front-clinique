import { Component, OnInit } from '@angular/core';

import { SalService } from '../../services/sal.service';
import { Sal } from './../../interface/sal';

@Component({
  selector: 'app-showsalarie',
  templateUrl: './showsalarie.component.html',
  styleUrls: ['./showsalarie.component.css']
})
export class ShowsalarieComponent implements OnInit {

  nom:string;
  sals: Sal[];
  
  resultSearch : any[] =[];
  constructor(private salService:SalService) {
this.getService();
  }



getService(){
  this.salService.get().subscribe((data:Sal[])=>{
   this.sals = data;
      }, (error)=>{
         console.log(error);
               alert('error');
                  });
}



ngOnInit() {
}
destroy(id){
 if(confirm('vous etes sure de suprimie ce personne')){
   this.salService.destroy(id).subscribe((data:Sal[])=>{
    this.sals = data;
    this.getService();
    console.log(data)
       }, (error)=>{
          console.log(error);
                alert('error');
                   });
 }
}

}
