import { Component, OnInit } from '@angular/core';
import { AuthService } from './../services/auth.service';
import { Router } from '@angular/router';
import { TokenService } from './../services/token.service';

@Component({
  selector: 'app-navigate',
  templateUrl: './navigate.component.html',
  styleUrls: ['./navigate.component.css']
})
export class NavigateComponent implements OnInit {

  public loggedIn: boolean=false;


  constructor(
    private Auth: AuthService,
    private router: Router,
    private Token: TokenService
  ) {

    let status = localStorage.getItem('loggedIn')
    console.log(status)
    if(status = 'true'){
      this.loggedIn = true;
    }else{
      this.loggedIn =false;
      
    }



  }

  ngOnInit() {
    this.Auth.authStatus.subscribe(value => this.loggedIn = value);
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    this.Auth.changeAuthStatus(false);
    this.router.navigateByUrl('/login');
  }


}
