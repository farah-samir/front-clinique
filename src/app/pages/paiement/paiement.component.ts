import { Component, OnInit } from '@angular/core';
import { OperationService } from '../../services/operation.service';
import { Operation } from './../../interface/operation';
import { PaiementService } from '../../services/paiement.service';
import { Paiement } from './../../interface/paiement';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.css']
})
export class PaiementComponent implements OnInit {

  pharmacie:string;
  monatnt_operation:string;
  montant_departement:string;
  montant_globale:number;


  paiement:Paiement = {
    operation_id: null,
    pharmacie:null,
    nomid:null,
    monatnt_operation:null,
    dates:null,
    montant_departement:null,
    montant_globale:null,
  
    };
    operation:Operation = {  nom_docteur: null,nom_maladie:null,genre:null,
      dates:null,pharmacie:null, mois:null, heure:null };
      
    editing : boolean = false;
    operations:Operation[];
    paiements:Paiement[];
    id: any;
    
      constructor(private paiementService:PaiementService , private operationService:OperationService ,
         private activatedRoute:ActivatedRoute , private router:Router) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.paiementService.get().subscribe((data:Paiement[])=>{
          this.paiements=data;
        this.paiement = this.paiements.find((m)=>{return m.id == this.id});
        console.log(this.paiement);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
      this.getService();
    
       }
      ngOnInit() {
      }
    


      getService(){
        this.operationService.get().subscribe((data:Operation[])=>{
         this.operations = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }




    
      savePaiement(){
      if (this.editing){
    
        this.paiement.montant_globale = parseFloat(this.paiement.montant_departement) 
        + parseFloat(this.paiement.pharmacie )
        + parseFloat(this.paiement. monatnt_operation) ;
        console.log(this.paiement.montant_globale);
    
        this.paiementService.put(this.paiement).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/shpowpaiement']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
        this.paiement.montant_globale = parseFloat(this.paiement.montant_departement) 
        + parseFloat(this.paiement.pharmacie )
        + parseFloat(this.paiement. monatnt_operation) ;
        console.log(this.paiement.montant_globale);
     
        this.paiementService.save(this.paiement).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/shpowpaiement']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }
    
    }
    

}
