import { Component, OnInit } from '@angular/core';
import { SpaieService } from '../../services/spaie.service';
import { Salarie } from './../../interface/salarie';
import { SalarieService } from '../../services/salarie.service';
import { Spaie } from './../../interface/spaie';
import { ActivatedRoute, Router } from '@angular/router';
import { nbLocale } from 'ngx-bootstrap';
@Component({
  selector: 'app-paie',
  templateUrl: './paie.component.html',
  styleUrls: ['./paie.component.css']
})
export class PaieComponent implements OnInit {


  salaire_net:number;
  salaire:string;
  prime:string;



  spaie:Spaie = {
    salarie_id: null,
    specialite:null,
    salaire:null,
    prime:null,
    salaire_net:null,
    cin:null,
    prenom:null,
   
    };
    salarie:Salarie = {
      nom: null,
     metie:null,
    
     specialite:null,
      };
      
    editing : boolean = false;
    spaies:Spaie[];
    salaries:Salarie[];
    id: any;
    
      constructor(private categoryService:SpaieService , private salarieService:SalarieService ,
         private activatedRoute:ActivatedRoute , private router:Router) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.categoryService.get().subscribe((data:Spaie[])=>{
          this.spaies=data;
        this.spaie = this.spaies.find((m)=>{return m.id == this.id});
        console.log(this.spaie);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
      this.getService();
    
       }
      ngOnInit() {
      }
    


      getService(){
        this.salarieService.get().subscribe((data:Salarie[])=>{
         this.salaries = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }




    
      savePaie(){
      if (this.editing){
    
        this.spaie.salaire_net = parseFloat(this.spaie.salaire) + parseFloat(this.spaie.prime) ;
        console.log(this.spaie.salaire_net);

    
        this.categoryService.put(this.spaie).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showpaie']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
        this.spaie.salaire_net = parseFloat(this.spaie.salaire) + parseFloat(this.spaie.prime) ;
        console.log(this.spaie.salaire_net);
     
        this.categoryService.save(this.spaie).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showpaie']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }
    
    }
    
    

}
