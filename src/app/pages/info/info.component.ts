import { Component, OnInit } from '@angular/core';
import { InfoService } from '../../services/info.service';
import { Info } from './../../interface/info';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  info:Info = {
    cin:null,
    prenom:null,
    nom:null,
    adresse:null,
    telephone:null,
    telephone_mb:null,
    date_red_v:null,
   
    };
    editing : boolean = false;
    infos:Info[];
    id: any;
    
      constructor(private infoService:InfoService , private activatedRoute:ActivatedRoute , private router:Router) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.infoService.get().subscribe((data:Info[])=>{
          this.infos=data;
        this.info = this.infos.find((m)=>{return m.id == this.id});
        console.log(this.info);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
    
    
       }
      ngOnInit() {
      }
    
    
      saveInfo(){
      if (this.editing){
    
    
    
        this.infoService.put(this.info).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showsinfo']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
     
        this.infoService.save(this.info).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showsinfo']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }
    
    }
    

}
