import { Component, OnInit } from '@angular/core';
import { OperationService } from '../../services/operation.service';
  import { Operation } from './../../interface/operation';
  import { StockService } from '../../services/stock.service';
  import { Stock } from './../../interface/stock';
  import { AnalyseService } from '../../services/analyse.service';
  import { Analyse } from './../../interface/analyse';
  import { ActivatedRoute, Router } from '@angular/router';
  import { SalarieService } from '../../services/salarie.service';
  import { Salarie } from './../../interface/salarie';


@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.css']
})
export class OperationComponent implements OnInit {

  
  analyse:Analyse = { rendez_id:null, nomid:null, depart_id: null, chambre: null, test: null, date_red_v: null, };
  
  stock:Stock = {
    nomid: null,
    achat_id:null,
    categorie_id:null,
    prix:null,
    nobrachat:null,
    tva:null,
    montant:null,
   
    };
    salarie:Salarie = {
      nom: null,metie:null, specialite:null,
      }; 
operation:Operation = {  nom_docteur: null,nom_maladie:null,genre:null,dates:null,pharmacie:null, mois:null, heure:null };
  
      editing : boolean = false;
      analyses:Analyse[];
      id: any;
      operations:Operation[];
      stocks:Stock[];
      salaries:Salarie[];
        constructor ( private operationService:OperationService , private stockService:StockService  ,
           private salarieService:SalarieService ,
           private activatedRoute:ActivatedRoute , private router:Router , private analyseService:AnalyseService) {
      
          this.id = this.activatedRoute.snapshot.params['id'];
        if(this.id){
          this.editing = true;
          this.operationService.get().subscribe((data:Operation[])=>{
            this.operations=data;
          this.operation = this.operations.find((m)=>{return m.id == this.id});
          console.log(this.operation);
          },(error)=>{
            console.log(error);
          });
        }else {
          this.editing = false;
        }
      
      this.getAnalyse();
      this.getDepartement();
      this.getSpaie();
         }
        ngOnInit() {
        }
        getAnalyse(){
          this.analyseService.get().subscribe((data:Analyse[])=>{
           this.analyses = data;
              }, (error)=>{
                 console.log(error);
                       alert('error');
                          });
        }
  
        getDepartement(){
          this.stockService.get().subscribe((data:Stock[])=>{
           this.stocks = data;
              }, (error)=>{
                 console.log(error);
                       alert('error');
                          });
        }
  
        getSpaie(){
          this.salarieService.get().subscribe((data:Salarie[])=>{
           this.salaries = data;
              }, (error)=>{
                 console.log(error);
                       alert('error');
                          });
        }
        saveOperation(){
        if (this.editing){
      
          this.operationService.put(this.operation).subscribe((data)=>{
            alert('enregistrement actualisé');
            this.router.navigate(['/showoperation']);
            console.log(data);
          },(error)=>{
            console.log(error);
            alert('eroor');
          });
      
      
        }else {
    
       
          this.operationService.save(this.operation).subscribe((data)=>{
            alert('enregistrement save');
            this.router.navigate(['/showoperation']);
            console.log(data);
          },(error)=>{
            console.log(error);
            alert('eroor');
          });
      
        }
  
  }

}
