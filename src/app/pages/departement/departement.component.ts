import { Component, OnInit } from '@angular/core';
import { DepartementService } from '../../services/departement.service';

import { Departement } from './../../interface/departement';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-departement',
  templateUrl: './departement.component.html',
  styleUrls: ['./departement.component.css']
})
export class DepartementComponent implements OnInit {

  departement:Departement = {
    nom_department: null,
    chambre:null,
   
    };
    editing : boolean = false;
    departements:Departement[];
    id: any;
    
      constructor(private departService:DepartementService , private activatedRoute:ActivatedRoute , private router:Router) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.departService.get().subscribe((data:Departement[])=>{
          this.departements=data;
        this.departement = this.departements.find((m)=>{return m.id == this.id});
        console.log(this.departement);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
    
    
       }
      ngOnInit() {
      }
    
    
      saveDepart(){
      if (this.editing){
    
    
    
        this.departService.put(this.departement).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showCatgeorie']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
     
        this.departService.save(this.departement).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showCatgeorie']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }
    
    }
    

}
