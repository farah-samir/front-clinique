import { Component, OnInit } from '@angular/core';
import { RendezService } from '../../services/rendez.service';
import { Rendez } from './../../interface/rendez';
import { DepartementService } from '../../services/departement.service';
import { Departement } from './../../interface/departement';
import { AnalyseService } from '../../services/analyse.service';
import { Analyse } from './../../interface/analyse';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-analyse',
  templateUrl: './analyse.component.html',
  styleUrls: ['./analyse.component.css']
})
export class AnalyseComponent implements OnInit {

  analyse:Analyse = { rendez_id:null,nomid: null, depart_id: null, chambre: null, test: null, date_red_v: null, };

    rendez:Rendez = {   cin:null, prenom:null, nom:null, heure:null, analyses:null, date_red_v:null, };
      departement:Departement = {  nom_department: null,chambre:null,  };

    editing : boolean = false;
    analyses:Analyse[];
    id: any;
    rendezs:Rendez[];
    departements:Departement[];

      constructor ( private rendezService:RendezService , private departementService:DepartementService
        , private activatedRoute:ActivatedRoute , private router:Router , private analyseService:AnalyseService) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.analyseService.get().subscribe((data:Analyse[])=>{
          this.analyses=data;
        this.analyse = this.analyses.find((m)=>{return m.id == this.id});
        console.log(this.analyse);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
    
    this.getRendez();
    this.getDepartement();
       }
      ngOnInit() {
      }
      getRendez(){
        this.rendezService.get().subscribe((data:Rendez[])=>{
         this.rendezs = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }

      getDepartement(){
        this.departementService.get().subscribe((data:Departement[])=>{
         this.departements = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }


      saveAnalyse(){
      if (this.editing){
    
        this.analyseService.put(this.analyse).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showanalyse']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
  
     
        this.analyseService.save(this.analyse).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showanalyse']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }

}


}
