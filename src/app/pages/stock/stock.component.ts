import { Component, OnInit } from '@angular/core';
import { AchatService } from '../../services/achat.service';
import { Achat } from './../../interface/achat';
import { VenduService } from '../../services/vendu.service';
import { Vendu } from './../../interface/vendu';
import { StockService } from '../../services/stock.service';
import { Stock } from './../../interface/stock';
import { ActivatedRoute, Router } from '@angular/router';
import { CategorieService } from '../../services/categorie.service';
import { Categorie } from './../../interface/categorie';

@Component({
  selector: 'app-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.css']
})
export class StockComponent implements OnInit {

  tvaprix:string;
  prix:string;
  nobrachat:string;
  tva:string;
  montant:number ;

  categorie:Categorie = { nom:null,  };
  categories:Categorie[];


  stock:Stock = {
    nomid: null,
    achat_id:null,
    categorie_id:null,
    prix:null,
    nobrachat:null,
    tva:"0.2",
    montant:null,
   
    };
    achat:Achat = { nom: null,prix: null, nobrachat: null, categorie_id: null,
     
      };
      vendu:Vendu = { nom: null,prix: null, nobrachat: null, categorie_id: null,
     
      };
    editing : boolean = false;
    stocks:Stock[];
    id: any;
    achats:Achat[];
    vendus:Vendu[];
      constructor(private achatService:AchatService ,private venduService:VenduService ,private categoryService:CategorieService ,
        private activatedRoute:ActivatedRoute , private router:Router , private stockService:StockService) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.stockService.get().subscribe((data:Stock[])=>{
          this.stocks=data;
        this.stock = this.stocks.find((m)=>{return m.id == this.id});
        console.log(this.stock);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
    
    this.getAchat();
    this.getVendu();
    this.getService()
       }
      ngOnInit() {
      }
      getAchat(){
        this.achatService.get().subscribe((data:Achat[])=>{
         this.achats = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }

      getVendu(){
        this.venduService.get().subscribe((data:Vendu[])=>{
         this.vendus = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }

      getService(){
        this.categoryService.get().subscribe((data:Categorie[])=>{
         this.categories = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }

      saveStcok(){
      if (this.editing){
       
    
        this.stockService.put(this.stock).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showstock']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
       
         this.stock.montant = (( parseFloat( this.stock.prix ) 
         * parseFloat( this.stock.tva ) ) + parseFloat( this.stock.prix )) * parseFloat( this.stock.nobrachat ) ;

     
     
        this.stockService.save(this.stock).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showstock']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }

}

}
