import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

import { SalService } from '../../services/sal.service';
import { SalarieService } from '../../services/salarie.service';

import { Salarie } from './../../interface/salarie';
import { Sal } from '../../interface/sal';


@Component({
  selector: 'app-sal',
  templateUrl: './sal.component.html',
  styleUrls: ['./sal.component.css']
})
export class SalComponent implements OnInit {


  salarie:Salarie = {
    nom: null,
   metie:null,
  
   specialite:null,
    };
    sal:Sal = {
     salarie_id: null,
     date_e:null,
     date_s:null,
     genreconge:null,
     congeformation:null,
       congecitoyen:null,
       specialite:null,
       prenom:null,
  
      };
      editing : boolean = false;
  sals : Sal[];
  id: any;
  salaries: Salarie[];
 

  constructor(private salarieService:SalarieService , private salService:SalService, 
    private activatedRoute:ActivatedRoute , private router:Router
    ) {

    this.id = this.activatedRoute.snapshot.params['id'];
  if(this.id){
    this.editing = true;
    this.salarieService.get().subscribe((data:Sal[])=>{
      this.sals=data;
    this.sal = this.sals.find((m)=>{return m.id == this.id});
    console.log(this.sal);
    },(error)=>{
      console.log(error);
    });
  }else {
    this.editing = false;
  }
  this.getService();

  }

  ngOnInit() {
    
  }

  getService(){
    this.salarieService.get().subscribe((data:Salarie[])=>{
     this.salaries = data;
        }, (error)=>{
           console.log(error);
                 alert('error');
                    });
  }


  saveSal(){
    if (this.editing){
  
      this.salService.put(this.sal).subscribe((data)=>{
        alert('salaire a été modifié ');
        this.router.navigate(['/showconge']);
         
        console.log(data);
      },(error)=>{
        console.log(error);
        alert('eroor');
      });


    }else {
    

      this.salService.save(this.sal).subscribe((data)=>{
        alert('nouveau salaire ');
        this.router.navigate(['/showconge']) ;
       console.log(data);
      },(error)=>{
        console.log(error);
        alert('eroor');
      });

    }

  }



}
