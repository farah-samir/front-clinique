import { Component, OnInit } from '@angular/core';
import { CategorieService } from '../../services/categorie.service';
import { Categorie } from './../../interface/categorie';
import { ActivatedRoute, Router } from '@angular/router';
import { AchatService } from '../../services/achat.service';
import { Achat } from './../../interface/achat';

@Component({
  selector: 'app-achat',
  templateUrl: './achat.component.html',
  styleUrls: ['./achat.component.css']
})
export class AchatComponent implements OnInit {

  categorie:Categorie = {
    nom: null,
   
    };
    achat:Achat = { nom: null,prix: null, nobrachat: null, categorie_id: null,
     
      };
    editing : boolean = false;
    categories:Categorie[];
    id: any;
    achats:Achat[];
      constructor(private achatService:AchatService
        ,private categoryService:CategorieService , private activatedRoute:ActivatedRoute , private router:Router) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.achatService.get().subscribe((data:Achat[])=>{
          this.achats=data;
        this.achat = this.achats.find((m)=>{return m.id == this.id});
        console.log(this.achat);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
    
    this.getService();
       }
      ngOnInit() {
      }
      getService(){
        this.categoryService.get().subscribe((data:Categorie[])=>{
         this.categories = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }
    
      saveAchat(){
      if (this.editing){
    
    
    
        this.achatService.put(this.achat).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showCatgeorie']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
     
        this.achatService.save(this.achat).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showCatgeorie']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }

}
}
