import { Component, OnInit } from '@angular/core';
import { CategorieService } from '../../services/categorie.service';
import { Categorie } from './../../interface/categorie';
import { ActivatedRoute, Router } from '@angular/router';
import { VenduService } from '../../services/vendu.service';
import { Vendu } from './../../interface/vendu';
@Component({
  selector: 'app-vende',
  templateUrl: './vende.component.html',
  styleUrls: ['./vende.component.css']
})
export class VendeComponent implements OnInit {

  categorie:Categorie = {
    nom: null,
   
    };
    vendu:Vendu = { nom: null,prix: null, nobrachat: null, categorie_id: null,
     
      };
    editing : boolean = false;
    categories:Categorie[];
    vendus:Vendu[];
    id: any;
    
    
      constructor(private venduService:VenduService
        ,private categoryService:CategorieService , private activatedRoute:ActivatedRoute , private router:Router) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.venduService.get().subscribe((data:Vendu[])=>{
          this.vendus=data;
        this.vendu = this.vendus.find((m)=>{return m.id == this.id});
        console.log(this.vendu);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
    
    this.getService();
       }
      ngOnInit() {
      }
      getService(){
        this.categoryService.get().subscribe((data:Categorie[])=>{
         this.categories = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }
    
      saveVendu(){
      if (this.editing){
    
    
    
        this.venduService.put(this.vendu).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showvendu']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
     
        this.venduService.save(this.vendu).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showvendu']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }

}

}
