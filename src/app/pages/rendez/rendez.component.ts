import { Component, OnInit } from '@angular/core';
import { RendezService } from '../../services/rendez.service';
import { Rendez } from './../../interface/rendez';
import { ActivatedRoute, Router } from '@angular/router';
import { InfoService } from '../../services/info.service';
import { Info } from './../../interface/info';

@Component({
  selector: 'app-rendez',
  templateUrl: './rendez.component.html',
  styleUrls: ['./rendez.component.css']
})
export class RendezComponent implements OnInit {

  info:Info = {
    cin:null,
    prenom:null,
    nom:null,
    adresse:null,
    telephone:null,
    telephone_mb:null,
    date_red_v:null,
   
    };
    rendez:Rendez = {
      cin:null,
      prenom:null,
      nom:null,
      heure:null,
      analyses:null,
      date_red_v:null,
     
      };
    editing : boolean = false;
    infos:Info[];
    rendezs:Rendez[];
    id: any;
    
      constructor(private infoService:InfoService , private rendezService:RendezService , 
        private activatedRoute:ActivatedRoute , private router:Router) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.rendezService.get().subscribe((data:Rendez[])=>{
          this.rendezs=data;
        this.rendez = this.rendezs.find((m)=>{return m.id == this.id});
        console.log(this.rendez);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
   this.getService();
    
       }
      ngOnInit() {
      }
    
      getService(){
        this.infoService.get().subscribe((data:Info[])=>{
         this.infos = data;
            }, (error)=>{
               console.log(error);
                     alert('error');
                        });
      }
    
      saveRendez(){
      if (this.editing){
    
        this.rendezService.put(this.rendez).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showrendez']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
     
        this.rendezService.save(this.rendez).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showrendez']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }
    
    }

}
