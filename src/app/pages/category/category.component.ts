import { ActivatedRoute, Router } from '@angular/router';
import { CategorieService } from '../../services/categorie.service';
import { Categorie } from './../../interface/categorie';
import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

 
  categorie:Categorie = {
    nom: null,
   
    };
    editing : boolean = false;
    categories:Categorie[];
    id: any;
    
      constructor(private categoryService:CategorieService , private activatedRoute:ActivatedRoute , private router:Router) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.categoryService.get().subscribe((data:Categorie[])=>{
          this.categories=data;
        this.categorie = this.categories.find((m)=>{return m.id == this.id});
        console.log(this.categorie);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
    
    
       }
      ngOnInit() {
      }
    
    
      saveCategory(){
      if (this.editing){
    
    
    
        this.categoryService.put(this.categorie).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showCatgeorie']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
     
        this.categoryService.save(this.categorie).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showCatgeorie']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }
    
    }
    
    

  

}
