import { Component, OnInit } from '@angular/core';
import { SalarieService } from '../../services/salarie.service';
import { Salarie } from './../../interface/salarie';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-salarie',
  templateUrl: './salarie.component.html',
  styleUrls: ['./salarie.component.css']
})
export class SalarieComponent implements OnInit {

  salarie:Salarie = {
    nom: null,
   metie:null,
   
   specialite:null,
    };
    editing : boolean = false;
    salaries:Salarie[];
    id: any;
    
      constructor(private salarieService:SalarieService , private activatedRoute:ActivatedRoute , private router:Router) {
    
        this.id = this.activatedRoute.snapshot.params['id'];
      if(this.id){
        this.editing = true;
        this.salarieService.get().subscribe((data:Salarie[])=>{
          this.salaries=data;
        this.salarie = this.salaries.find((m)=>{return m.id == this.id});
        console.log(this.salarie);
        },(error)=>{
          console.log(error);
        });
      }else {
        this.editing = false;
      }
    
    
       }
      ngOnInit() {
      }
    
    
      saveSalarie(){
      if (this.editing){
    
    
    
        this.salarieService.put(this.salarie).subscribe((data)=>{
          alert('enregistrement actualisé');
          this.router.navigate(['/showsalaries']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
    
      }else {
     
        this.salarieService.save(this.salarie).subscribe((data)=>{
          alert('enregistrement save');
          this.router.navigate(['/showsalaries']);
          console.log(data);
        },(error)=>{
          console.log(error);
          alert('eroor');
        });
    
      }
    
    }

}
