import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Stock } from '../interface/stock';


@Injectable({
  providedIn: 'root'
})
export class StockService {

  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/stock');
    }

  save(stock:Stock){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/stock',stock, {headers: headers});
  }

  put(stock:Stock){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/stock/' + stock.id , stock, {headers: headers});
  }


  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/stock/'+ id);

  }
}
