import { TestBed } from '@angular/core/testing';

import { SpaieService } from './spaie.service';

describe('SpaieService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpaieService = TestBed.get(SpaieService);
    expect(service).toBeTruthy();
  });
});
