import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Salarie } from '../interface/salarie';

@Injectable({
  providedIn: 'root'
})
export class SalarieService {

   
  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/salarie');
    }

  save(salarie:Salarie){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/salarie',salarie, {headers: headers});
  }
  put(salarie:Salarie){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/salarie/' + salarie.id , salarie, {headers: headers});
  }
  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/salarie/'+ id);

  }
}
