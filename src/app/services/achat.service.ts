import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Achat } from '../interface/achat';

@Injectable({
  providedIn: 'root'
})
export class AchatService {

  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/achat');
    }

  save(achat:Achat){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/achat',achat, {headers: headers});
  }

  put(achat:Achat){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/achat/' + achat.id , achat, {headers: headers});
  }


  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/achat/'+ id);

  }
}
