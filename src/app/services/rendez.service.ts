import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Rendez } from '../interface/rendez';

@Injectable({
  providedIn: 'root'
})
export class RendezService {

  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/rendez');
    }

  save(rendez:Rendez){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/rendez',rendez, {headers: headers});
  }
  put(rendez:Rendez){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/rendez/' + rendez.id , rendez, {headers: headers});
  }
  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/rendez/'+ id);

  }
}
