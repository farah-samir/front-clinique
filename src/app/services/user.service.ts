import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../interface/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/user');
    }

  save(user:User){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/user', user, {headers: headers});
  }
  put(user:User){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/user/' + user.id , user, {headers: headers});
  }
  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/user/'+ id);

  }
}
