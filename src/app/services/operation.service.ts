import { Injectable } from '@angular/core';
import { Operation } from '../interface/operation';
import { HttpClient, HttpHeaders } from '@angular/common/http';
 

@Injectable({
  providedIn: 'root'
})
export class OperationService {

 
    API_ENDPOINT ='http://localhost:8000/api';
  
    constructor(private httpClient:HttpClient) { }
      get() {
      return this.httpClient.get(this.API_ENDPOINT+'/operation');
      }
  
    save(operation:Operation){
      const headers = new HttpHeaders({'Content-Type':'application/json'});
      return this.httpClient.post(this.API_ENDPOINT + '/operation',operation, {headers: headers});
    }
    put(operation:Operation){
      const headers = new HttpHeaders({'Content-Type':'application/json'});
      return this.httpClient.put(this.API_ENDPOINT + '/operation/' + operation.id , operation, {headers: headers});
    }
    destroy(id){
        return this.httpClient.delete(this.API_ENDPOINT+'/operation/'+ id);
  
    }
}
