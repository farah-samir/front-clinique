import { Injectable } from '@angular/core';
import { Paiement } from '../interface/paiement';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PaiementService {

  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/paiement');
    }

  save(paiement:Paiement){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/paiement',paiement, {headers: headers});
  }
  put(paiement:Paiement){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/paiement/' + paiement.id , paiement, {headers: headers});
  }
  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/paiement/'+ id);

  }
}
