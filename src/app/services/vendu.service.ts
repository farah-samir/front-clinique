import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Vendu } from '../interface/vendu';

@Injectable({
  providedIn: 'root'
})
export class VenduService {

  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/vendu');
    }

  save(vendu:Vendu){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/vendu',vendu, {headers: headers});
  }

  put(vendu:Vendu){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/vendu/' + vendu.id , vendu, {headers: headers});
  }


  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/vendu/'+ id);

  }


}
