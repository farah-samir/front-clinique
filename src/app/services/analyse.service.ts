import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Analyse } from '../interface/analyse';

@Injectable({
  providedIn: 'root'
})
export class AnalyseService {

  
  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/analyse');
    }

  save(analyse:Analyse){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/analyse',analyse, {headers: headers});
  }
  put(analyse:Analyse){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/analyse/' + analyse.id , analyse, {headers: headers});
  }
  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/analyse/'+ id);

  }

}
