import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Spaie } from '../interface/spaie';

@Injectable({
  providedIn: 'root'
})
export class SpaieService {

  
  API_ENDPOINT ='http://localhost:8000/api';

  constructor(private httpClient:HttpClient) { }
    get() {
    return this.httpClient.get(this.API_ENDPOINT+'/spaie');
    }

  save(spaie:Spaie){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.post(this.API_ENDPOINT + '/spaie',spaie, {headers: headers});
  }
  put(spaie:Spaie){
    const headers = new HttpHeaders({'Content-Type':'application/json'});
    return this.httpClient.put(this.API_ENDPOINT + '/spaie/' + spaie.id , spaie, {headers: headers});
  }
  destroy(id){
      return this.httpClient.delete(this.API_ENDPOINT+'/spaie/'+ id);

  }
}
