import { TestBed } from '@angular/core/testing';

import { VenduService } from './vendu.service';

describe('VenduService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VenduService = TestBed.get(VenduService);
    expect(service).toBeTruthy();
  });
});
